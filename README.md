# Vonage Messages Connector
The Dispatch API enables the developer to specify a multiple message workflow.

Documentation: https://developer.vonage.com/messages/overview

## Prerequisites

+ Vonage account
+ API Key and Secret for Basic Authentication

## Supported Operations
**All endpoints are operational.**
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

